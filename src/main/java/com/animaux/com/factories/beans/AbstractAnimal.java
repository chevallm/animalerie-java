package com.animaux.com.factories.beans;

public abstract class AbstractAnimal {

	private String nom;
	private int poids;
	private String race;	
	
	public int getPoids() {
		return poids;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	@Override
	public String toString() {
		return "L'animal s'appelle " + nom + ", il pèse " + poids;
	}

}
