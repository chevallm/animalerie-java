package com.animaux.com.factories.beans;

import com.animaux.com.factories.interfaces.Animal;

public class Oiseau extends AbstractAnimal implements Animal {	

	private int altitudeDeVol;
	
	public Oiseau(String nom, Integer poids, String race) {
		super.setNom(nom);
		super.setPoids(poids);
		super.setRace(race);
	}
	
	public String communiquer() {
		return "Cui-Cui";
	}

	@Override
	public String toString() {
		return super.getNom() + " est un " + this.getClass().getSimpleName().toLowerCase() + " (" + this.getRace() + ") de " + super.getPoids() + "kg et il dit '" + this.communiquer() + "'. Il vole à " + this.getAltitudeDeVol() + " mètres d'altitude.";
	}
	
	public int getAltitudeDeVol() {
		return altitudeDeVol;
	}

	public void setAltitudeDeVol(int altitudeDeVol) {
		this.altitudeDeVol = altitudeDeVol;
	}	

}
