package com.animaux.com.factories.beans;

import com.animaux.com.factories.interfaces.Animal;

public class Chat extends AbstractAnimal implements Animal {
	
	private String jouetPrefere;

	public Chat(String nom, Integer poids, String race) {
		super.setNom(nom);
		super.setPoids(poids);
		super.setRace(race);
	}
	
	public String communiquer() {
		return "Miaaaaou";		
	}

	@Override
	public String toString() {
		return super.getNom() + " est un " + this.getClass().getSimpleName().toLowerCase() + " (" + this.getRace() + ") de " + super.getPoids() + "kg et il dit '" + this.communiquer() + "'. Son jouet préféré est " + this.getJouetPrefere() + ".";
	}

	public String getJouetPrefere() {
		return jouetPrefere;
	}

	public void setJouetPrefere(String jouetPrefere) {
		this.jouetPrefere = jouetPrefere;
	}
	
}
