package com.animaux.com.factories.beans;

import com.animaux.com.factories.interfaces.Animal;

public class Chien extends AbstractAnimal implements Animal {
	
	private int vitesse;
	
	public Chien(String nom, Integer poids, String race) {
		super.setNom(nom);
		super.setPoids(poids);
		super.setRace(race);
	}

	public String communiquer() {
		return "Wooaf";
	}

	@Override
	public String toString() {
		return super.getNom() + " est un " + this.getClass().getSimpleName().toLowerCase() + " (" + this.getRace() + ") de " + super.getPoids() + "kg et il dit '" + this.communiquer() + "'. Il cours à " + this.getVitesse() + "km/h.";
	}

	public int getVitesse() {
		return vitesse;
	}

	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}

}
