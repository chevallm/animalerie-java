package com.animaux.com.factories.impl;

import com.animaux.com.factories.AbstractAnimalFactory;
import com.animaux.com.factories.beans.Chat;
import com.animaux.com.factories.interfaces.Animal;

public class ChatFactoryImpl extends AbstractAnimalFactory {
	
	public Animal genererUnChat(String nom, int poids, String race, String jouetPrefere) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Chat LeChat = (Chat) this.genererAnimal(Chat.class.getName(), nom, poids, race);
		LeChat.setJouetPrefere(jouetPrefere);
		return LeChat;
	}
}
