package com.animaux.com.factories.impl;

import com.animaux.com.factories.AbstractAnimalFactory;
import com.animaux.com.factories.beans.Chien;
import com.animaux.com.factories.interfaces.Animal;

public class ChienFactoryImpl extends AbstractAnimalFactory {
	
	public Animal genererUnChien(String nom, int poids, String race, int vitesse) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Chien leChien = (Chien) this.genererAnimal(Chien.class.getName(), nom, poids, race);
		leChien.setVitesse(vitesse);
		return leChien;
	}
}
