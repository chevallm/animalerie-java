package com.animaux.com.factories.impl;

import com.animaux.com.factories.AbstractAnimalFactory;
import com.animaux.com.factories.beans.Oiseau;
import com.animaux.com.factories.interfaces.Animal;

public class OiseauFactoryImpl extends AbstractAnimalFactory {

	public Animal genererUnOiseau(String nom, int poids, String race, int altitudeDeVol) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Oiseau Oiseau = (Oiseau) this.genererAnimal(Oiseau.class.getName(), nom, poids, race);
		Oiseau.setAltitudeDeVol(altitudeDeVol);
		return Oiseau;
	}
}
