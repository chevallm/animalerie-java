package com.animaux.com.factories.interfaces;

public interface Animal {
	public String communiquer();
}
