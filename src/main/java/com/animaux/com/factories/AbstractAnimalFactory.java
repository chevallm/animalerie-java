package com.animaux.com.factories;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.animaux.com.factories.interfaces.Animal;

public abstract class AbstractAnimalFactory {

	public Animal genererAnimal(String leNomDeLaClasse, String nom, Integer poids, String race)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Animal animalAGenerer = null;
		try {			
			@SuppressWarnings("unchecked")
			Class<Animal> classeAnimalAGenerer = (Class<Animal>) Class.forName(leNomDeLaClasse);
			Constructor<Animal>  constructorAnimal = classeAnimalAGenerer.getConstructor((new Class[] {Class.forName("java.lang.String"),Class.forName("java.lang.Integer"), Class.forName("java.lang.String")}));
			animalAGenerer = constructorAnimal.newInstance(new Object[] {nom, poids, race});
		} catch(ClassNotFoundException cnfex) {
			System.out.println(cnfex.getMessage());
		} catch(InstantiationException iex) {
			System.out.println(iex.getMessage());
		} catch (IllegalAccessException iaex) {
			System.out.println(iaex.getMessage());
		} catch (SecurityException sex) {
			System.out.println(sex.getMessage());
		} catch (NoSuchMethodException nsmex) {
			System.out.println(nsmex.getMessage());
		} catch (IllegalArgumentException iaex) {
			System.out.println(iaex.getMessage());
		} catch (InvocationTargetException itex) {
			System.out.println(itex.getMessage());
		}
		
		return animalAGenerer;
	}
}
