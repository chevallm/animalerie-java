package com.animaux.com;

import java.io.InputStream;
import java.util.ArrayList;


import java.util.Iterator;

import javax.json.*;
import com.animaux.com.factories.impl.ChatFactoryImpl;
import com.animaux.com.factories.impl.ChienFactoryImpl;
import com.animaux.com.factories.impl.OiseauFactoryImpl;
import com.animaux.com.factories.interfaces.Animal;


public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
    	
    	ArrayList<Animal> listeDAnimaux = new ArrayList<Animal>();
    	ChienFactoryImpl cfi = new ChienFactoryImpl();
    	ChatFactoryImpl chfi = new ChatFactoryImpl();
    	OiseauFactoryImpl ofi = new OiseauFactoryImpl();
    	
    	System.out.println("Bienvue à l'animalerie.");
    	
    	try {
			InputStream is = App.class.getResourceAsStream("ressources/animals.json");
			JsonReader jr = Json.createReader(is);
			
			JsonObject obj = jr.readObject();
			JsonArray results =  obj.getJsonArray("data");
			for(JsonObject result : results.getValuesAs(JsonObject.class)) {
				
				if(result.getString("class").equals("Chien")) {
					listeDAnimaux.add(cfi.genererUnChien(result.getString("nom"), result.getInt("poids"), result.getString("race"), result.getInt("special")));
				} else if(result.getString("class").equals("Chat")) {
					listeDAnimaux.add(chfi.genererUnChat(result.getString("nom"), result.getInt("poids"), result.getString("race"), result.getString("special")));
				} else if(result.getString("class").equals("Oiseau")) {
					listeDAnimaux.add(ofi.genererUnOiseau(result.getString("nom"), result.getInt("poids"), result.getString("race"), result.getInt("special")));
				}
			}
		} catch (NullPointerException npex) {
			System.err.println(npex.getClass().getSimpleName());
		}
    	
    	Iterator<Animal> iteratorForAnimal = listeDAnimaux.iterator();
    	
    	if(listeDAnimaux.size() < 1) {
    		System.err.println("Il n'y a pas d'animal aujourd'hui.");
    	} else {
    		while(iteratorForAnimal.hasNext()) {
    			System.out.println(iteratorForAnimal.next().toString());
    		}
    		
    	}
    	System.out.println("Bonne journée à l'animalerie !");
    	
    }
}
